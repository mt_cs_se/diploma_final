import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AdminMain from './pages/AdminMain';
import AdminLogin from './components/admin-login/admin-login';
import './App.css';


function App() {
  const isLogged = true;
  return (
      <div>
        {isLogged ? <AdminMain/> : <AdminLogin/>}
      </div>
  );
}

export default App;
