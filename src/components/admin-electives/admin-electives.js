import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import './admin-electives.css';
import {DataGrid, Column, SearchPanel, RequiredRule,
  Editing} from 'devextreme-react/data-grid';
import FileUploader from 'devextreme-react/file-uploader';
import {useEffect, useState} from "react";
import {publicRequest} from "../../api/requestMethods";

function AdminElectives() {

  const [courses,setCourses] = useState([]);

  const fetchCourses = async() => {
    try{
      const response = await publicRequest.get("")
      setCourses(response.data);
    }catch (e) {
      console.log(e)
    }
  }

  useEffect(()=>{
    fetchCourses();
  },[])

  const employees = [{
    "ElectiveID": 1,
    "ElectiveName": "Applied Applications Analysis",
    "Syllabus": "Syllabus_AAA",
    "Teachers": "Yerbolat Kalpakov"
  }, {
    "ElectiveID": 2,
    "ElectiveName": "Network Applications",
    "Syllabus": "Syllabus_NAT",
    "Teachers": "Zhanshuak Zhaibergenova"
  }];
  const fileExtensions = ['.pdf'];
  return (
    <div className="admin-electives">
      <DataGrid
        dataSource={employees}
        keyExpr="ElectiveID"
        columnAutoWidth={true}
        showBorders={true}
        rowAlternationEnabled={true}
        showRowLines={true}
      >
        <SearchPanel visible={true} />
        <Column dataField="ElectiveID" dataType="text" width={120}>
          <RequiredRule />
        </Column>
        <Column dataField="ElectiveName" dataType="text" width={320}>
          <RequiredRule />
        </Column>
        <Column dataField="Syllabus" dataType="text" width={320}>
          <RequiredRule />
        </Column>
        <Column dataField="Teachers" dataType="text" width={300}>
          <RequiredRule />
        </Column>
        <Editing
          mode="popup"
          allowUpdating={true}
          allowDeleting={true}
          allowAdding={true}
        />
      </DataGrid>
    </div>
  );
}

export default AdminElectives;
