import './students-stat.css';
import PieChart, {
    Series,
    Label,
    Connector,
    Size,
    Export
  } from 'devextreme-react/pie-chart';

function StudentsStat() {
  const firstYear = [{
    "Name": "Students who took electives",
    "Total": 350
  }, {
    "Name": "Students who didn't take electives",
    "Total": 242
  }];

  const secondYear = [{
    "Name": "Students who took electives",
    "Total": 298
  }, {
    "Name": "Students who didn't take electives",
    "Total": 250
  }];
  return (
    <div className='student-stat'>
      <div className='first'>
        <PieChart
          id="pie"
          dataSource={firstYear}
          palette="Bright"
          title="1st year students"
        >
          <Series argumentField="Name" valueField="Total">
            <Label visible={true}>
              <Connector visible={true} width={1} />
            </Label>
          </Series>

          <Size width={500} />
          <Export enabled={true} />
        </PieChart>
      </div>
      <div className='second'>
        <PieChart
          id="pie"
          dataSource={secondYear}
          palette="Ocean"
          title="2nd year students"
        >
          <Series argumentField="Name" valueField="Total">
            <Label visible={true}>
              <Connector visible={true} width={1} />
            </Label>
          </Series>

          <Size width={500} />
          <Export enabled={true} />
        </PieChart>
      </div>    
    </div>
  );
}

export default StudentsStat;
