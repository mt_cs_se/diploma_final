import './admin-statistics.css';
import {useEffect, useState} from 'react';
import {Dropdown} from 'react-bootstrap';

import StudentsStat from './students-stat';
import ElectivesStat from './electives-stat';
import GroupsStat from './groups-stat';

function AdminStatistics() {
  const [value, setValue] = useState("StudentsStat");

  const handleOnChange = (e) => {
    setValue(e.target.value);
  };

  const [studentStat, setStudentStat] = useState(false);
  const [electiveStat, setElectiveStat] = useState(false);
  const [groupStat, setGroupStat] = useState(false);

  useEffect( () => {
    value === "StudentsStat" ? setStudentStat(true) : setStudentStat(false);
    value === "ElectivesStat" ? setElectiveStat(true) : setElectiveStat(false);
    value === "GroupsStat" ? setGroupStat(true) : setGroupStat(false);
  }, [value]);
  return (
    <div className="admin-statistics">
      <select className='form-select' value={value} onChange={handleOnChange}>
        <option value="StudentsStat">Statistics on who took electives</option>
        <option value="ElectivesStat">Statistics on electives</option>
        <option value="GroupsStat">Statistics on groups</option>
      </select>
      <br></br>
      {studentStat && <StudentsStat/>}
      {electiveStat && <ElectivesStat/>}
      {groupStat && <GroupsStat/>}
    </div>
  );
}

function Tab({value}){
  if (value.toString() === 'StudentsStat') {
    return <StudentsStat />;
  } else if (value.toString() === 'ElectivesStat') {
    return <ElectivesStat />;
  } else if (value.toString() === 'GroupsStat') {
    return <GroupsStat />;
  }
}

export default AdminStatistics;
