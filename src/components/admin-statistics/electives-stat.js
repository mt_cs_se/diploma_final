import './electives-stat.css';
import {
    Chart, SeriesTemplate, CommonSeriesSettings, Title, Export
  } from 'devextreme-react/chart';

function ElectivesStat() {
  const areas1 = [
    { elective: "IOS", number: 59 },
    { elective: "Python", number: 120 },
    { elective: "Java", number: 90 },
    { elective: "Android", number: 12 },
  ];

  const areas2 = [
    { elective: "AAA", number: 76 },
    { elective: "Research Methods", number: 176 },
    { elective: "Design Thinking", number: 90 },
    { elective: "Communication", number: 37 },
  ];
  return (
    <div className='elective-stat'>
        <div className='first'>
            <Chart
            id="chart1"
            palette="Soft"
            dataSource={areas1}>
            <CommonSeriesSettings
            argumentField="elective"
            valueField="number"
            type="bar"
            ignoreEmptyPoints={true}
            />
            <SeriesTemplate nameField="elective" />
            <Title
            text="Electives that were chosen by 1st year students"
            />
            <Export
                    enabled={true}
                    printingEnabled={false}
                />
            </Chart>
        </div>
        <div className='second'>
            <Chart
            id="chart2"
            palette="Violet"
            dataSource={areas2}>
            <CommonSeriesSettings
            argumentField="elective"
            valueField="number"
            type="bar"
            ignoreEmptyPoints={true}
            />
            <SeriesTemplate nameField="elective" />
            <Title
            text="Electives that were chosen by 2nd year students"
            />
            <Export
                    enabled={true}
                    printingEnabled={false}
                />
            </Chart>
        </div>
    </div>
  );
}

export default ElectivesStat;
