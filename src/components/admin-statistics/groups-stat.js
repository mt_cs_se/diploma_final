import './groups-stat.css';
import {
    Chart, SeriesTemplate, CommonSeriesSettings, Title, Export
  } from 'devextreme-react/chart';

function GroupsStat() {
  const areas1 = [
    { elective: "AAA", number: 7 },
    { elective: "Research Methods", number: 18 },
    { elective: "Design Thinking", number: 6 },
    { elective: "Communication", number: 12 },
  ];

  const areas2 = [
    { elective: "IOS", number: 9 },
    { elective: "Python", number: 11 },
    { elective: "Java", number: 15 },
    { elective: "Android", number: 4 },
  ];
  return (
    <div className='group-stat'>
        <div className='first'>
            <Chart
            id="chart1"
            palette="Green Mist"
            dataSource={areas1}>
            <CommonSeriesSettings
            argumentField="elective"
            valueField="number"
            type="bar"
            ignoreEmptyPoints={true}
            />
            <SeriesTemplate nameField="elective" />
            <Title
            text="Electives that were chosen by IT-1901"
            />
            <Export
                    enabled={true}
                    printingEnabled={false}
                />
            </Chart>
        </div>
        <div className='second'>
            <Chart
            id="chart2"
            palette="Soft Blue"
            dataSource={areas2}>
            <CommonSeriesSettings
            argumentField="elective"
            valueField="number"
            type="bar"
            ignoreEmptyPoints={true}
            />
            <SeriesTemplate nameField="elective" />
            <Title
            text="Electives that were chosen by IT-2001"
            />
            <Export
                    enabled={true}
                    printingEnabled={false}
                />
            </Chart>
        </div>
    </div>
  );
}

export default GroupsStat;
