import {Component} from 'react';
import UserIcon from './default-user-image.png';

import './admin-profile.css';

class AdminProfile extends Component {
    state = {
        profileImg: UserIcon
    }
    imageHandler = (e) => {
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState == 2){
                this.setState({profileImg: reader.result})
            }
        }
        reader.readAsDataURL(e.target.files[0])
    }
    render (){
        const {profileImg} = this.state;
        const admin = [
            {name: "Admin", surname: "Adminov", email: "admin@astanait.edu.kz",
            password: "*******"}
        ];
        return (
            <div className="admin-profile">
                <div>
                    <img class="image" src={profileImg} alt="Profile pic"/>
                    <br></br>
                    <input type="file" name="image-upload" id="input" accept="image/*" onChange={this.imageHandler}/>
                    <div className="label">
                        <label htmlFor="input" className="image-upload">Choose your photo</label>
                    </div>
                </div>
                <div className="user-info">
                    <p>Name: <span>{admin[0].name}</span></p>
                    <p>Surname: <span>{admin[0].surname}</span></p>
                    <p>Email: <span>{admin[0].email}</span></p>
                    <p>Password: <span>{admin[0].password}</span></p>
                </div>  
            </div>
        );
    }
}

export default AdminProfile;