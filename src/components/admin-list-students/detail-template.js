import React from 'react';
import { DataGrid, Column } from 'devextreme-react/data-grid';
import ArrayStore from 'devextreme/data/array_store';
import DataSource from 'devextreme/data/data_source';

const students = [{
    "StudentID": 1,
    "StudentName": "Adlet",
    "StudentSurname": "Social",
    "StudentGroup": "IT-1901",
    "ChosenElectives": "Network Applications"
  }, {
    "StudentID": 2,
    "StudentName": "Alibek",
    "StudentSurname": "Social",
    "StudentGroup": "IT-1902",
    "ChosenElectives": "Network Applications"
  }];

class DetailTemplate extends React.Component {
    constructor(props) {
        super(props);
        this.dataSource = getDetail(props.data.key);
      }    
  render() {
    return (
      <React.Fragment>
        <DataGrid
          dataSource={this.dataSource}
          showBorders={true}
          columnAutoWidth={true}
        >
            <Column dataField="ChosenElectives" dataType="text"/>
        </DataGrid>
      </React.Fragment>
    );
  }
}
function getDetail(key) {
    return new DataSource({
      store: new ArrayStore({
        data: students,
        key: 'StudentID',
      }),
      filter: ['StudentID', '=', key],
    });
  }
export default DetailTemplate;
