import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import './admin-list-students.css';
import DetailTemplate from './detail-template';
import {DataGrid, Column, SearchPanel, RequiredRule,
  Editing, MasterDetail} from 'devextreme-react/data-grid';

function AdminListStudents() {
  const students = [{
    "StudentID": 1,
    "StudentName": "Adlet",
    "StudentSurname": "Social",
    "StudentGroup": "IT-1901",
    "ChosenElectives": "Network Applications"
  }, {
    "StudentID": 2,
    "StudentName": "Alibek",
    "StudentSurname": "Social",
    "StudentGroup": "IT-1902",
    "ChosenElectives": "Network Applications"
  }];
  return (
    <div className="admin-list-students">
      <DataGrid
        dataSource={students}
        keyExpr="StudentID"
        columnAutoWidth={true}
        showBorders={true}
        rowAlternationEnabled={true}
        showRowLines={true}
      >
        <SearchPanel visible={true} />
        <Column dataField="StudentID" dataType="text" width={120}>
          <RequiredRule/>
        </Column>
        <Column dataField="StudentName" dataType="text" width={250}>
          <RequiredRule/>
        </Column>
        <Column dataField="StudentSurname" dataType="text" width={250}>
          <RequiredRule/>
        </Column>
        <Column dataField="ChosenElectives" dataType="text" width={400}>
          <RequiredRule/>
        </Column>
        <Editing
          mode="popup"
          allowUpdating={true}
          allowDeleting={true}
          allowAdding={true}
        />
      </DataGrid>
    </div>
  );
}

export default AdminListStudents;