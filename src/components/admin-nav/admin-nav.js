import {NavLink as Link} from 'react-router-dom';

import './admin-nav.css';

const AdminNav = () => {
    return (
        <div className="admin-nav col-sm">
            <ul className='nav-list'>
                <li><Link to="/" style={({ isActive }) => ({
                    color: isActive ? '#3DB166' : '#FFFFFF',
                    })}>Profile</Link></li>
                <li><Link to="/manage-electives" style={({ isActive }) => ({
                    color: isActive ? '#3DB166' : '#FFFFFF',
                    })}>Manage electives</Link></li>
                <li><Link to="/list-students" style={({ isActive }) => ({
                    color: isActive ? '#3DB166' : '#FFFFFF',
                    })}>List of students</Link></li>
                <li><Link to="/statistics" style={({ isActive }) => ({
                    color: isActive ? '#3DB166' : '#FFFFFF',
                    })}>Statistics</Link></li>
            </ul>
        </div>
    );
}

export default AdminNav;