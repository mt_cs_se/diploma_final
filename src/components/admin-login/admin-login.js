
import './admin-login.css';


function AdminLogin() {
  return (
    <div className='admin-login'>
      <div className='message-panel'>
        <p className='welcome'>Welcome back!</p>
        <p className='text-under'>Powered by Astana IT University</p>
      </div>
      <div className='form-panel'>
        <p className='login-text'>Login to your account</p>
        <p className='text-login'>Use your Outlook credentials</p>
        <form className='form-login'>
          <input type="text" placeholder="Email"></input><br></br>
          <input type="password" placeholder="Password"></input><br></br>
          <input type="submit" value="Login"></input>
        </form>
      </div>
    </div>
  );
}

export default AdminLogin;
