import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AdminNav from '../components/admin-nav/admin-nav';
import AdminProfile from '../components/admin-profile/admin-profile';
import AdminElectives from '../components/admin-electives/admin-electives';
import AdminListStudents from '../components/admin-list-students/admin-list-students';
import AdminStatistics from '../components/admin-statistics/AdminStatistics';
import { BsBoxArrowInRight as Exit} from "react-icons/bs";
import {NavLink as Link} from 'react-router-dom';
import './admin-main.css';


function AdminMain() {
  return (
    <Router>
      <div className="Main">
        <AdminNav/>
        <Routes>
          <Route path="/" element={<AdminProfile/>}/>
          <Route path="/manage-electives" element={<AdminElectives/>}/>
          <Route path="/list-students" element={<AdminListStudents/>}/>
          <Route path="/statistics" element={<AdminStatistics/>}/>
        </Routes>
        <Link to="/login"><Exit className='exitIcon'/></Link>
      </div>
    </Router>
  );
}

export default AdminMain;
