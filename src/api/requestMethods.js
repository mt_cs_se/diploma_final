import axios from "axios";

const BASE_URL = "https://api-healthcare-system.herokuapp.com/";

export const publicRequest = axios.create({
  baseURL: BASE_URL,
});

